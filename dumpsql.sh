#!/usr/bin/env bash
DB="wp_loftschool"
CURHOST="wordpress.loftschool"
PRODHOST="release.host.name"
mysqldump -uroot $DB > $DB.sql
cp $DB.sql $DB-prod.sql
sed -i s/$CURHOST/$PRODHOST/g $DB-prod.sql
