<?php
/*
 * Template Name: about
 */
get_header(); ?>

    <div class="content">
        <?php if( have_posts()) : while ( have_posts()) : the_post(); ?>

        <?php endwhile; else: ?>
        Ничего не найдено
        <?php endif; ?>
        <div class="ic">123</div>
        <div class="container_12">
            <div class="grid_4">
                <h2><?php echo get_field('title1'); ?></h2>
                <?php
                $image = get_field('image');
                ?>
                <img src="<?php echo $image['sizes']['medium'];?>" alt="" class="img_inner">
                <div class="text1 col2">
                    <a href="<?php the_field('link') ?>">Hom dui erosi laoru adipiscingq id risus sagittis, non consequat feiter milyno</a>
                </div>
                <?php the_content(); ?>
                <a href="<?php the_field('link') ?>" class="btn">more</a>
            </div>
            <div class="grid_4">
                <h2><?php echo get_field('title2'); ?></h2>
                <ul class="list1">
                    <?php
                    $args = array(
                        'post_type' => 'history',
                        'posts_per_page' => -1,
                        'orderby' => 'title',
                        'order' => 'asc'
                    );
                    $posts = query_posts($args);
                    ?>
                    <?php if( have_posts()) : while ( have_posts()) : the_post(); ?>
                        <li>
                            <time datetime="<?php the_time(); ?>"><?php the_title(); ?> -</time>
                            <div class="extra_wrapper">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_content(); ?>
                                </a>
                            </div>
                        </li>
                    <?php endwhile; else: ?>
                    Ничего не найдено
                    <?php endif; ?>
                    <?php
                    wp_reset_postdata();
                    wp_reset_query();
                    ?>
                </ul>
            </div>
            <div class="grid_4">
                <h2><?php echo get_field('title3'); ?></h2>
                <div class="text1 col2">
                    <a href="<?php the_field('link') ?>">Hom dui erosi laoru adipiscingq id risus sagittis, non consequat feiter milyno</a>
                </div>
                <?php the_field('opportunities'); ?>
            </div>
        </div>
    </div>
    <div class="gray_block center">
        <div class="container_12">
            <div class="grid_12">
                <h3>Best Professionals</h3>
            </div>
            <?php
            $args = array(
                'post_type' => 'people',
                'posts_per_page' => -1,
            );
            $posts = query_posts($args);
            ?>
            <?php if( have_posts()) : while ( have_posts()) : the_post(); ?>
                <div class="grid_3">
                    <div class="block3">
                        <?php the_post_thumbnail(); ?>
                        <div class="text2">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            <?php endwhile; else: ?>
            Ничего не найдено
            <?php endif; ?>
            <?php
            wp_reset_postdata();
            wp_reset_query();
            ?>
        </div>
    </div>

<?php get_footer();
