<?php get_header(); ?>
<div class="slider_wrapper">
    <div id="camera_wrap" class="">
        <div data-src="/images/slide.jpg"></div>
        <div data-src="/images/slide1.jpg"></div>
        <div data-src="/images/slide2.jpg"></div>
    </div>
</div>
<div class="container_12">
    <div class="grid_12">
        <div class="slogan">
            Our Ideas Will Raise Your Business Above the Expected <br>
            <a href="#" class="btn">more</a>
        </div>
    </div>
</div>
<div class="container_12">
    <section class="grid" id="grid">
        <a href="#" data-path-hover="m 180,70.57627 -180,0 L 0,0 180,0 z">
            <figure>
                <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 180,160 0,262 0,0 180,0 z"/></svg>
                <figcaption>
                    <div class="title">Consulting</div>
                </figcaption>
            </figure>
            <span>more</span>
        </a>
        <a href="#" data-path-hover="m 180,70.57627 -180,0 L 0,0 180,0 z">
            <figure>
                <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 180,160 0,262 0,0 180,0 z"/></svg>
                <figcaption>
                    <div class="title">Development</div>
                </figcaption>
            </figure>
            <span>more</span>
        </a>
        <a href="#" data-path-hover="m 180,70.57627 -180,0 L 0,0 180,0 z">
            <figure>
                <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 180,160 0,262 0,0 180,0 z"/></svg>
                <figcaption>
                    <div class="title">Analysis</div>
                </figcaption>
            </figure>
            <span>more</span>
        </a>
        <a href="#" data-path-hover="m 180,70.57627 -180,0 L 0,0 180,0 z">
            <figure>
                <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 180,160 0,262 0,0 180,0 z"/></svg>
                <figcaption>
                    <div class="title">Integration</div>
                </figcaption>
            </figure>
            <span>more</span>
        </a>
    </section>
</div>
<!--==============================Content=================================-->
<div class="content"><div class="ic">More Website Templates @ TemplateMonster.com - February 24, 2014!</div>
    <div class="container_12">
        <div class="grid_6">
            <img src="/images/page1_img1.jpg" alt="" class="img_inner fleft">
            <div class="extra_wrapper">
                <div class="title1">We Work for You!</div>
                <p>If you want to download this <span class="col3"><a href="http://blog.templatemonster.com/free-website-templates/" rel="dofollow">freebie</a></span>, visit TemplateMonster blog.</p>
                Want to find more themes of this kind? Go to <span class="col3"><a href="http://www.templatemonster.com/properties/topic/business-services/" rel="nofollow">Business and Services</a></span> website templates.
            </div>
            <div class="clear cl1"></div>
            In mollis erat mattisy neque facilisis, sit amet ultricieser erarutrum. Cras facilisis, nulla vel viverra auctor, leo magna sodales felis, quis
        </div>
        <div class="grid_3">
            <div class="block1">
                <div class="title">20 <span>Years of Experience</span></div>
                In mollis erat matt nequemer facilameteserylerarutrum. Cras facilisis, nulla
                <br>
                <a href="#" class="btn bt1">more</a>
            </div>
        </div>
        <div class="grid_3 ver">
            <div class="block1">
                <div class="title">18 <span>Partner Programs</span></div>
                Cras facilisis, nulla vel viverra auctor, leo magna sodaleel alesuada nibh odio ulit
                <br>
                <a href="#" class="btn bt1">more</a>
            </div>
        </div>
    </div>
</div>
<div class="gray_block">
    <div class="container_12">
        <div class="grid_4">
            <div class="block2">
                <time datetime="2014-01-01"><span class="col1">18</span>january</time>
                <div class="">
                    <div class="extra_wrapper">
                        <div class="title col1"><a href="#">Etiam dui ero laoretsiter golyn</a></div>
                    </div>
                </div>
                <div class="clear"></div>
                <p>Vivamus at magna non nunc tristiq oncus. Aliquam nibh ante, egestas id dicttuser</p>
                <a href="#" class="col1">read more</a>
            </div>
        </div>
        <div class="grid_4">
            <div class="block2">
                <time datetime="2014-01-01"><span class="col1">21</span>january</time>
                <div class="">
                    <div class="extra_wrapper">
                        <div class="title col1"><a href="#">Hom dui erosi laorufeiter milyno</a></div>
                    </div>
                </div>
                <div class="clear"></div>
                <p>Non nunc tristique ous. Aliqum nibh ante, egestas id dictumctuser liberoraesnt</p>
                <a href="#" class="col1">read more</a>
            </div>
        </div>
        <div class="grid_4">
            <div class="block2">
                <time datetime="2014-01-01"><span class="col1">12</span>february</time>
                <div class="">
                    <div class="extra_wrapper">
                        <div class="title col1"><a href="#">Joui eros, laorulberer golyno</a></div>
                    </div>
                </div>
                <div class="clear"></div>
                <p>Vivamus at magna non nunc tristique os. Aliquam nibh ante, egestas id dicuser</p>
                <a href="#" class="col1">read more</a>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>