<?php
/*
 * Template Name: blogposts
 */
get_header(); ?>
<div class="content cont2"><div class="ic">More Website Templates @ TemplateMonster.com - February 24, 2014!</div>
    <div class="container_12">
        <div class="grid_12">
            <h2 class="mb0"><?php the_title(); ?></h2>
        </div>
    </div>
</div>
<div class="gray_block gb1">
    <div class="container_12">
        <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 2,
        );
        $posts = query_posts($args);
        ?>
        <div class="grid_8">
            <?php if( have_posts()) : while ( have_posts()) : the_post(); ?>
            <div class="blog">
                <?php the_post_thumbnail(); ?>
                <div class="extra_wrapper">
                    <div class="text1">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <time class="col2" datetime="<?php the_date();?>"><?php the_date();?></time>
                    </div>
                    <?php the_excerpt(); ?>
                    <br>
                    <a href="<?php the_permalink(); ?>" class="btn">more</a>
                </div>
            </div>
            <?php endwhile; else: ?>
                Ничего не найдено
            <?php endif; ?>

            <?php
            the_posts_pagination();
            wp_reset_query();
            wp_reset_postdata();
            ?>
        </div>
        <div class="grid_4">
            <div class="text1 col3 head1">Categories:</div>
            <?php
            $categories = get_categories();
            ?>
            <ul class="list l1">
                <?php foreach($categories as $category) : ?>
                <li><a href="<?php echo get_term_link($category,'category'); ?>"><?=$category->name?></a></li>
                <?php endforeach; ?>

            </ul>
            <div class="text1 head1 col3">
                Recent Posts:
            </div>
            <?php query_posts('posts_per_page=2'); ?>
            <?php if( have_posts()) : while ( have_posts()) : the_post(); ?>

            <div class="post">
                <div class="fl">
                    <?php //the_post_thumbnail(); ?>
                    <time datetime="<?php the_time();?>"><?php the_time();?></time>
                </div>
                <div class="extra_wrapper">
                    <a href="<?php the_permalink();?>">
                        <?php the_excerpt();?>
                    </a>
                </div>
            </div>

            <?php endwhile; else: ?>
                Ничего не найдено
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer();