<?php  get_header(); ?>

    <div class="content cont2">
        <?php get_template_part('partials/breadcrumbs'); ?>
        <div class="container_12">
            <div class="grid_12">
                <h2 class="mb0"><?php the_title(); ?></h2>
                <?php if( have_posts()) : while ( have_posts()) : the_post(); ?>

                    <div class="content_wp">
                        <?php the_content(); ?>
                    </div>

                <?php endwhile; else: ?>
                    Ничего не найдено
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php
get_template_part('partials/socials');
?>
<?php get_footer();
