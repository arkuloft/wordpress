<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_loftschool');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HG,4UmPNT=3W0&T[_-8[1> BHV;T;r-.!h|HMu/7]@W{1)6TA]|%v]&_*1^b(uho');
define('SECURE_AUTH_KEY',  '|-D18~Z3:JT!54(-H6`&DF/.V6}@ avKu=^ITRT1-+z!^&>j!wX$A2D~H1lxTCF|');
define('LOGGED_IN_KEY',    'xEIXUQ>1]_w`O%>a3~N-KnrDg#=umnnd|+pW1I/CZ,=-j;L8iSdgR[||W|b7!R@y');
define('NONCE_KEY',        '45t7&(%%b&U]{F8p Z+R)hxTtTh*G>[NWFb}#}au%UN1bX(mWr-(,tgS)qOz,(FN');
define('AUTH_SALT',        'WEE0wtU[v<dC<3x_99<ViK--AzciMfZh 0+/j}Unnx=~>|g:r9w!*^#Hf<sCut <');
define('SECURE_AUTH_SALT', '+Kk<0l5<]gQgb<R-Tj:tPJI|C#7bv~a(CcHiHV=86#kQ0B@2Di)q@%_(#u2I)v.[');
define('LOGGED_IN_SALT',   'OEzAt/aZms8NHj@j;5_fJc630LMjvDIjPBVxI5,AiFP#Y;2~<]U|e+3W+6H!,]6E');
define('NONCE_SALT',       'K4$R1nn*4_g.]Pa_eY7swT-U-bVK<#!I$!@GX4P9LSmFOT<2K&NXo)|g+e%+5S=I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
